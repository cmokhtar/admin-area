<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Target Material Design Bootstrap Admin Template</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/materialize/css/materialize.min.css') }}" media="screen,projection" />
        <!-- Bootstrap Styles-->
        <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" />
        <!-- FontAwesome Styles-->
        <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" />
        <!-- Morris Chart Styles-->
        <link href="{{ asset('assets/js/morris/morris-0.4.3.min.css') }}" rel="stylesheet" />
        <!-- Custom Styles-->
        <link href="{{ asset('assets/css/custom-styles.css') }}" rel="stylesheet" />
        <!-- Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <link rel="stylesheet" href="{{ asset('assets/js/Lightweight-Chart/cssCharts.css') }}">
        @yield('css')
    </head>
<body>
    <div id="wrapper">
        @include('includes._navbar')
        @include('includes._sidebar')
        <div class="content-wrapper">
            <div id="page-wrapper">
                <div class="header">
            @include('includes._header')
                </div>

                <div id="page-inner">
                @yield('content')

                <div id="page-inner">
                    <div class="dashboard-cards">
                @include('includes._footer')
                    </div>
                </div>
            </div>

        </div>



    </div>

     <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>

	<!-- Bootstrap Js -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

	<script src="{{ asset('assets/materialize/js/materialize.min.js') }}"></script>

    <!-- Metis Menu Js -->
    <script src="{{ asset('assets/js/jquery.metisMenu.js') }}"></script>
    <!-- Morris Chart Js -->
    <script src="{{ asset('assets/js/morris/raphael-2.1.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/morris/morris.js') }}"></script>


	<script src="{{ asset('assets/js/easypiechart.js') }}"></script>
	<script src="{{ asset('assets/js/easypiechart-data.js') }}"></script>

	 <script src="{{ asset('assets/js/Lightweight-Chart/jquery.chart.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/custom-scripts.js') }}"></script>
    @yield('js')
</body>
</html>
